var createPathToMap;
$(document).ready(function(){
	
	var map = [];
	//build map
	
	for(y=0;y<10;y++) {
		for(x=0;x<10;x++) {
			$('.map').append('<div id="'+x+'_'+y+'"></div>');
		}
	}
	// globals saved to local variables
	var m_random = Math.random;
	var m_floor = Math.floor;
	
	
	var $tiles = $('.map > div');

	var minLoc = 0;
	var maxLoc = 9;
	var startX = 0;
	var startY = 0;
	var endX = 9;
	var endY = 9;
	var currX = startX;
	var currY = startY;
	var minPathLength = 20;
	var maxPathLength = 75;
	var path = [];
	//var undoedPath = [];
	var visitedPath = [];
	var excludedTiles = [
		[0,3],
		[0,4],
		[0,5],
		[0,6],
		[0,7],
		[0,8],
		[0,9],
		[3,0],
		[4,0],
		[5,0],
		[6,0],
		[7,0],
		[8,0],
		[9,0],
		[9,1],
		[9,2],
		[9,3],
		[9,4],
		[9,5],
		[9,6],
		[1,9],
		[2,9],
		[3,9],
		[4,9],
		[5,9],
		[6,9]
	];
	
	function getLocation(tile) {
		var matches = tile.attr('id').match(/(\d+)_(\d+)/);
		return [Number(matches[1]), Number(matches[2])];
	}
	
	// init map
	$tiles.each(function() {
		var $this = $(this)
		var loc = getLocation($this);
		var x = loc[0];
		var y = loc[1];
		map[x] = map[x] || [];
		map[x][y] = $this;
	});
	
	function getTile(x,y) {
		return map[x][y];
	}
	
	function checkValue(val) {
		val = ((val > maxLoc) ? maxLoc : val );
		val = ((val < minLoc) ? minLoc : val );
		return val;
	}
	
	function randomBetween(min, max) {
		min = checkValue(min);
		max = checkValue(max);
		return m_floor(m_random() * (max - min + 1)) + min;
	}
	
	function pushPath(x,y) {
		var i = path.length - 1;
		if (i > -1) 
			getTile(path[i][0], path[i][1]).removeClass('end'); 
		getTile(x, y).addClass('visit path end');
		path.push([x,y]);
	}
	
	function popPath() {
		var index = path.length - 1
		if (index > 0) {
			getTile(path[index][0], path[index][1]).removeClass('path end');
			index--;
			getTile(path[index][0], path[index][1]).addClass('end');
			return path.pop();
			
		}
		// Path generation has failed because undoing starting point. Restarting.
		return undefined;
		//return path.pop();
	}
	
	function undoPath() {
		var index = path.length - 1;
		
		if (index > 1) {
			currX = path[index-1][0];
			currY = path[index-1][1];
		}
		else {
			currX = path[0][0];
			currY = path[0][1];
		}
		
		var pathLoc = popPath();
		if (typeof pathLoc == "undefined") {
			console.log("path gen failed, resetting");
			resetMap();
		}
		else {
			//undoedPath.push(pathLoc);
		}
	}
	
	
	
	function isValidLocation(loc) {
		var locX = loc[0];
		var locY = loc[1];
		if (loc[0] == currX && loc[1] == currY) {
			return false;
		}
		
		/*for (i = undoedPath.length-1; i > -1; i--) { 
			var pathX = undoedPath[i][0];
			var pathY = undoedPath[i][1];
			
			if(pathX == locX && pathY == locY) {
				return false
			}
		}*/
		
		for (i = excludedTiles.length-1; i > -1; i--) { 
			var pathX = excludedTiles[i][0];
			var pathY = excludedTiles[i][1];
			
			if(pathX == locX && pathY == locY) {
				return false
			}
		}
		
		for (i = path.length-2; i > -1; i--) { 
			var pathX = path[i][0];
			var pathY = path[i][1];
			
			if(pathX == locX && pathY == locY) {
				return false
			}
			else if (pathX == checkValue(locX - 1) && pathY == locY) {
				return false
			}
			else if (pathX == checkValue(locX + 1) && pathY == locY) {
				return false
			}
			else if (pathX == locX && pathY == checkValue(locY - 1)) {
				return false
			}
			else if (pathX == locX && pathY == checkValue(locY + 1)) {
				return false
			}
		}
		return true;
	}
	
	// temp variables from last recursived function calls
	var inValidValues = [];

	function alreadyChecked(axis, direction) {
		
		for (i = inValidValues.length-1; i > -1; i--) {
			var vals = inValidValues[i];
			if (vals[0] == axis && vals[1] == direction)
				return true;
		}
		return false;
	}
	
	function isVisited(loc) {
		var locX = loc[0];
		var locY = loc[1];
		
		for (i = visitedPath.length-1; i > -1; i--) { 
			var pathX = visitedPath[i][0];
			var pathY = visitedPath[i][1];
			
			if(pathX == locX && pathY == locY) {
				return true;
			}
		}
		return false;
	}
	
	function getNewLocation(x, y) {
		var loc = [x, y];
		var axis = randomBetween(0,1);
		var direction = ((randomBetween(0, 1) > 0) ? 1 : -1);
		
		// Every direction is invalid, going back from path
		if (inValidValues.length > 3 ) {
			undoPath();
			inValidValues = null;
			inValidValues = [];
			return getNewLocation(currX, currY);
		}
		
		while (alreadyChecked(axis, direction)) {
			axis = randomBetween(0,1);
			direction = ((randomBetween(0, 1) > 0) ? 1 : -1);
		}
		
		var newLoc = loc[axis] + direction;
		loc[axis] = checkValue(newLoc);
		if (!isValidLocation(loc)) {
			inValidValues.push([axis, direction]);
			return getNewLocation(x, y);
		}
		else if(isVisited(loc)) {
			undoPath();
			inValidValues = null;
			inValidValues = [];
			return getNewLocation(currX, currY);
		}
		else {
			inValidValues = null;
			inValidValues = [];
			//undoedPath = null;
			//undoedPath = [];
			visitedPath.push(loc);
			return loc;
		}
		
	}
	
	function isPathFinished() {
		var val = ((currX == endX) && (currY == endY));
		$('#completed').attr('hidden', !val);
		return val;
	}
	
	function buildPath() {
		var newLoc = getNewLocation(currX, currY) ;
		var newX = newLoc[0];
		var newY = newLoc[1];
		currX = newX;
		currY = newY;
		pushPath(currX, currY);
		
		return isPathFinished();
	}
	
	function resetMap() {
		$tiles.removeClass('visit path end');
		currX = startX;
		currY = startY;
		//undoedPath = null;
		//undoedPath = [];
		inValidValues = null;
		inValidValues = [];
		visitedPath = null;
		visitedPath = [];
		path = null;
		path = [];
		pushPath(currX, currY);
	}

	function loopBuild() {
		resetMap();
		while (!buildPath()) {}
	}
	
	function checkPathLength(minpl, maxpl) {
		var l = path.length;
		console.log(l, minpl, maxpl);
		return (l > minpl && l < maxpl);
	}
	
	function pathToSamu() {
		function compareNumbers(a, b) {
			return a - b;
		}
		var samuArr = [];
		for (i = 0; i < path.length; i++) { 
			samuArr.push(path[i][0]+path[i][1]*10);
		}
		return samuArr.sort(compareNumbers);
	}
	
	createPathToMap = function (numObjectives) {
		var minpl = numObjectives * 2; // minimum pathlength
		var maxpl = m_floor(minpl * 2);
		minpl = ((minpl < minPathLength) ? minPathLength : minpl);
		minpl = ((minpl > maxPathLength) ? maxPathLength-10 : minpl);
		maxpl = ((maxpl <= minPathLength) ? minPathLength+10 : maxpl);
		maxpl = ((maxpl > maxPathLength) ? maxPathLength : maxpl);
		loopBuild();
		while (!checkPathLength(minpl, maxpl)) { loopBuild(); }

		return pathToSamu();
	}
	
	
	build = buildPath;
	restart = resetMap;
	loop = loopBuild;
	undo = undoPath;
});